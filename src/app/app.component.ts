import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { interval } from 'rxjs';
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

	secondes: number;
	counterSubscription: Subscription;

  	ngOnInit() {
    	const counter = interval(1000);

	    counter.subscribe(
	      (value) => {
	        this.secondes = value;
	      },
	      (error) => {
	        console.log('Uh-oh, an error occurred! : ' + error);
	      },
	      () => {
	        console.log('Observable complete!');
	      }
	    );

  	}

  	ngOnDestroy() {
  		this.counterSubscription.unsubscribe(); // destroy the subscription
  	}
}

